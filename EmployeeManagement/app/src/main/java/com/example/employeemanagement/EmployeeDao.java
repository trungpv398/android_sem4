package com.example.employeemanagement;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface EmployeeDao {
    @Insert(onConflict = REPLACE)
    void insertEmployee(EmployeeEntity employeeEntity);

    @Update
    void updateEmployee(EmployeeEntity employeeEntity);

    @Delete
    void deleteEmployee(EmployeeEntity employeeEntity);

    @Query("SELECT * FROM Employee")
    List<EmployeeEntity> getAllBookmark();

    @Query("SELECT * FROM Employee WHERE id = :id")
    EmployeeEntity getBookmark(int id);

    @Query("DELETE  FROM Employee")
    void deleteAll();

}
