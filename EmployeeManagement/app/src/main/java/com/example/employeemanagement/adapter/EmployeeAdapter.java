package com.example.employeemanagement.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.employeemanagement.EmployeeEntity;
import com.example.employeemanagement.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class EmployeeAdapter extends RecyclerView.Adapter {
    Activity activity;
    List<EmployeeEntity> list;

    public EmployeeAdapter(Activity activity, List<EmployeeEntity> list) {
        this.activity = activity;
        this.list = list;
    }

    public void reloadData(List<EmployeeEntity> list)
    {
        this.list = list;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.activity_list_employee,parent,false);
        EmployeeHolder holder = new EmployeeHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        EmployeeHolder hd = (EmployeeHolder) holder;
        EmployeeEntity employeeEntity = list.get(position);
        hd.tvEmployeeName.setText(employeeEntity.employeename);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class EmployeeHolder extends RecyclerView.ViewHolder{
        TextView tvEmployeeName;

        public EmployeeHolder(@NonNull  View itemView) {
            super(itemView);
            tvEmployeeName = (TextView) itemView.findViewById(R.id.tvEmployeeName);
        }
    }
}
