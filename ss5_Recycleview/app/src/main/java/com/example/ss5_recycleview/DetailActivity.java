package com.example.ss5_recycleview;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        int img = intent.getIntExtra("IMG",0);
        String desc = intent.getStringExtra("DESC");
        String price = intent.getStringExtra("PRICE");
        TextView tvName = findViewById(R.id.tvName);
        ImageView tvImg = findViewById(R.id.ivCover);
        TextView tvDesc = findViewById(R.id.tvDes);
        TextView tvPrice = findViewById(R.id.tvPrice);
        tvName.setText(name);
        tvImg.setImageResource(img);
        tvDesc.setText(desc);
        tvPrice.setText(price);

    }
}
