package com.example.ss5_recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IOnclickItem{
    List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        ProductAdapter adapter = new ProductAdapter(this,listProduct,this);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);

        RecyclerView rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData()
    {
        listProduct.add(new Product("Zara P1","Love Shift 1","1100.000",R.drawable.p1));
        listProduct.add(new Product("Zara P2","Love Shift 1","1100.000",R.drawable.p2));
        listProduct.add(new Product("Zara P3","Love Shift 1","1100.000",R.drawable.p3));
        listProduct.add(new Product("Zara P4","Love Shift 1","1100.000",R.drawable.p4));
        listProduct.add(new Product("Zara P5","Love Shift 1","1100.000",R.drawable.p5));
        listProduct.add(new Product("Zara P6","Love Shift 1","1100.000",R.drawable.p1));
        listProduct.add(new Product("Zara P7","Love Shift 1","1100.000",R.drawable.p2));
        listProduct.add(new Product("Zara P8","Love Shift 1","1100.000",R.drawable.p3));
        listProduct.add(new Product("Zara P9","Love Shift 1","1100.000",R.drawable.p4));
        listProduct.add(new Product("Zara P10","Love Shift 1","1100.000",R.drawable.p5));
    }

    private void goToDetail(String name,int img,String price,String desc)
    {
        Intent intent = new Intent(this,DetailActivity.class);
        intent.putExtra("NAME",name);
        intent.putExtra("IMG",img);
        intent.putExtra("PRICE",price);
        intent.putExtra("DESC",desc);
        startActivity(intent);
    }

    @Override
    public void onClickItem(int position) {
        Product product = listProduct.get(position);
        Toast.makeText(this,product.getTitle(),Toast.LENGTH_SHORT).show();
        goToDetail(product.getTitle(),product.getImg(),product.getPrice(),product.getDes());
    }
}