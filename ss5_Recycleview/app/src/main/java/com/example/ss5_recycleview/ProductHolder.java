package com.example.ss5_recycleview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductHolder extends RecyclerView.ViewHolder {
    public ImageView ivCover;
    public TextView tvTitle;
    public TextView tvDes;
    public TextView tvPrice;

    public ProductHolder(View itemView) {
        super(itemView);
        ivCover = (ImageView) itemView.findViewById(R.id.ivCover);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        tvDes = (TextView) itemView.findViewById(R.id.tvDes);
        tvPrice = (TextView)  itemView.findViewById(R.id.tvPrice);
    }

    public ProductHolder( View itemView, ImageView ivCover, TextView tvTitle, TextView tvDes, TextView tvPrice) {
        super(itemView);
        this.ivCover = ivCover;
        this.tvTitle = tvTitle;
        this.tvDes = tvDes;
        this.tvPrice = tvPrice;
    }

}
