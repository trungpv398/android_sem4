package com.example.ss7apiweather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.os.Bundle;
import android.widget.TextView;

import com.example.ss7apiweather.adapter.HourAdapter;
import com.example.ss7apiweather.model.Weather;
import com.example.ss7apiweather.network.ApiManager;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvHour;
    private TextView tvTem;
    private TextView tsStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTem = (TextView) findViewById(R.id.tvTem);
        tsStatus = (TextView) findViewById(R.id.tvStatus);


        //B1
        getHours();
        //B2
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        //B3

        //B4
        rvHour = (RecyclerView)findViewById(R.id.rcHour);
        rvHour.setLayoutManager(layoutManager);

    }

    private void getHours()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiManager service = retrofit.create(ApiManager.class);
        service.getHour().enqueue(new Callback<List<Weather>>() {
            @Override
            public void onResponse(Call<List<Weather>> call, Response<List<Weather>> response) {
                if (response.body()==null)
                {
                    return;
                }
                List<Weather> listWeather = response.body();
                HourAdapter adapter = new HourAdapter(MainActivity.this,listWeather);
                rvHour.setAdapter(adapter);

                Weather weather = listWeather.get(0);
//                tvTem.setText( weather.getTemperature().getValue().intValue()+"0");
                tsStatus.setText(weather.getIconPhare());
                adapter.reloadData(listWeather);
            }

            @Override
            public void onFailure(Call<List<Weather>> call, Throwable t) {

            }
        });
    }
}