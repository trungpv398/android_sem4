package com.example.ss7apiweather.model;

public class Weather {
    private String DateTime;
    private int WeatherIcon;
    private String IconPhare;
    private Temperature temperature;

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public int getWeatherIcon() {
        return WeatherIcon;
    }

    public void setWeatherIcon(int weatherIcon) {
        WeatherIcon = weatherIcon;
    }

    public String getIconPhare() {
        return IconPhare;
    }

    public void setIconPhare(String iconPhare) {
        IconPhare = iconPhare;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }
}
