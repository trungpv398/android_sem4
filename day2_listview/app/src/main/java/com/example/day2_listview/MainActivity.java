package com.example.day2_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContacts,this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContacts.get(position);
                Toast.makeText(MainActivity.this,contactModel.getName(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData()
    {
        ContactModel contact = new ContactModel("Nguyen Van A","010212",R.drawable.f02);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van B","010212",R.drawable.female01);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van c","010212",R.drawable.female03);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van D","010212",R.drawable.male02);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van E","010212",R.drawable.male03);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van F","010212",R.drawable.male03);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van G","010212",R.drawable.female01);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van H","010212",R.drawable.f02);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van I","010212",R.drawable.male03);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen Van K","010212",R.drawable.female03);
        listContacts.add(contact);
    }
}