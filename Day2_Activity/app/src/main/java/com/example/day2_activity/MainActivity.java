package com.example.day2_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edUser;
    private EditText edPassword;
    private Button btLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btLogin:
                    onLogin();
                    break;
            default:
                break;
        }
    }

    private void onLogin()
    {
        if (edUser.getText().toString().isEmpty() || edPassword.getText().toString().isEmpty())
        {
            Toast.makeText(this,"Ban chua nhap user hoac password",Toast.LENGTH_SHORT).show();;

        }else{
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("USER_NAME",edUser.getText().toString());
            startActivity(intent);
        }
    }
}