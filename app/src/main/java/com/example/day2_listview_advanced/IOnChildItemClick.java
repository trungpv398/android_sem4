package com.example.day2_listview_advanced;

public interface IOnChildItemClick {
    void OnItemChildClick(int position);
}
