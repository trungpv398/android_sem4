package com.example.ss5recyclerview;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity extends AppCompatActivity {
    List<Contact> listContact = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //b1:data source
        initData();
        //b2: adapter
        ConatctAdapter adapter = new ConatctAdapter(listContact,this);
        //b3 layout manager

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,1);
        //b4: RecyclerView

        RecyclerView recyclerView = findViewById(R.id.rvContact);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initData()
    {
        listContact.add(new Contact("Nguyen phu trong","1212121",R.drawable.male01));
        listContact.add(new Contact("Nguyen phu A","1212121",R.drawable.male02));
        listContact.add(new Contact("Nguyen phu B","1212121",R.drawable.male03));
        listContact.add(new Contact("Nguyen phu C","1212121",R.drawable.male01));
        listContact.add(new Contact("Nguyen phu D","1212121",R.drawable.male02));
        listContact.add(new Contact("Nguyen phu E","1212121",R.drawable.male03));
    }
}