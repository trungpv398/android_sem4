package com.example.ss6_api_networking;

public class Item {
    private String date;
    private String title;
    private String image;
    private Content content;
    public Item() {
    }

    public Item(String date, String title, String image, Content content) {
        this.date = date;
        this.title = title;
        this.image = image;
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
