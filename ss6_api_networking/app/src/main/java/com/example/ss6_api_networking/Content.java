package com.example.ss6_api_networking;

public class Content {
    private String description;
   private String url;

    public Content() {
    }

    public Content(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String decription) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
